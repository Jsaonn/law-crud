from django.db import models

# Create your models here.

class Task(models.Model):
    name = models.CharField(max_length=200)
    desc = models.CharField(max_length=1000, null=True)
    deadline = models.DateField()
    isDone = models.BooleanField()

    def __str__(self):
        return self.name
