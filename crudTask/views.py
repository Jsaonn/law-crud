from django.shortcuts import render
from django.http import JsonResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import TaskSerializer
from .models import Task

# API Overview
@api_view(['GET'])
def api_overview(request):
    api_urls = {
        'List':'/task-list/',
        'Create':'/task-create/',
        'Update':'/task-update/<str:pk>/',
        'Delete':'/task-delete/<str:pk>/',
    }

    return Response(api_urls)

# API Task
@api_view(['GET'])
def task_list(request):
    tasks = Task.objects.all()
    serializer = TaskSerializer(tasks, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def task_create(request, *args, **kwargs):
    data = request.data
    new_task = Task.objects.create(
        name=data['name'],
        desc=data['desc'],
        deadline=data['deadline'],
        isDone=data['isDone']
    )
    new_task.save()
    serializer = TaskSerializer(new_task)
    return Response(serializer.data)

@api_view(['PUT'])
def task_update(request, pk):
    data = request.data
    task_obj = Task.objects.get(id=pk)
    serializer = TaskSerializer(instance=task_obj, data=data)
    serializer.is_valid(raise_exception=True)
    serializer.save()
    return Response(serializer.data)

@api_view(['DELETE'])
def task_delete(request, pk):
    task_obj = Task.objects.get(id=pk)
    task_obj.delete()
    return Response('Successfully delete task!')